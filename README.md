PlusPlus
================

Getting Started
----------------

We're working on getting our wiki setup, in the meantime refer to the LineageOS wiki.  

To initialize your local repository using the PlusPlus sources:

    repo init -u git://gitlab.com/TheNightman/plusplus.git

Then to pull the sources:

    repo sync

Please see the [LineageOS Wiki](https://wiki.lineageos.org/) for building instructions.

Supported Devices 
----------------------

PlusPlus currently runs only on:

* LGE Nexus 5

But we do plan to bring it to new devices as development continues!

Porting to your device
-----------------------

Since we are a new distrobution we need volunteers to help maintain PlusPlus for devices not currently supported. If  you get a build running, either create an issue or message me on XDA (thenightman).


